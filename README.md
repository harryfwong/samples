# Code Snippets

## WorkFusion 

### Platform Related

* **[CreateNodeFiles](https://bitbucket.org/harryfwong/samples/src/master/WorkFusion/CreateNodeFiles/)** PowerShell Script to create necessary node files and RDP files

## Jupyter Notebooks

Coded as Jupyter Notebooks to allow for easy hand-off open-source to users without programming experience

* **[RenameExcelVlookups](https://bitbucket.org/harryfwong/samples/src/master/Jupyter%20Notebooks/RenameExcelVlookups.ipynb)** Python code to modify vlookups in Excel