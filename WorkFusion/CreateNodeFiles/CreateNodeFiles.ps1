################ Params to Modify ######################
################ LEAVE ALL AS LOWERCASE ################
$node_env = "dev" ## dev | prod
$node_purpose = "project" ## project identification
$node_hub_server = "servername"

## Generate list rpa0001 ... rpa0101
$node_cwid_list = @() # declare empty array
1..101 | % { 
  $temp_cwid = ('rpa{0:d4}' -f $_)
  $node_cwid_list += $temp_cwid
}

################ Params to Modify ######################

foreach($node_cwid in $node_cwid_list) {
    $node_num = [int]$node_cwid.Substring(3,4)
    $node_port = 5100 + $node_num

    $node_logback_Template = [IO.File]::ReadAllText("$PSScriptRoot\templates\node_logback_template.txt")
    $node_logback_Template = $node_logback_Template.Replace("[[ node_cwid ]]","$node_cwid")


    $node_json_Template = [IO.File]::ReadAllText("$PSScriptRoot\templates\node_json_template.txt")
    $node_json_Template = $node_json_Template.Replace("[[ node_env ]]","$node_env")
    $node_json_Template = $node_json_Template.Replace("[[ node_purpose ]]","$node_purpose")
    $node_json_Template = $node_json_Template.Replace("[[ node_hub_server ]]","$node_hub_server")
    $node_json_Template = $node_json_Template.Replace("[[ node_cwid ]]","$node_cwid")
    $node_json_Template = $node_json_Template.Replace("[[ node_port ]]","$node_port")


    $node_batch_Template = [IO.File]::ReadAllText("$PSScriptRoot\templates\node_batch_template.txt")
    $node_batch_Template = $node_batch_Template.Replace("[[ node_cwid ]]","$node_cwid")

    #Write-Output $node_logback_Template
    #Write-Output $node_json_Template
    #Write-Output $node_batch_Template

    New-Item -Force -Path "$PSScriptRoot\configs\$node_hub_server" -Name "logback-node-$node_cwid.txt" -ItemType "file" -Value $node_logback_Template 
    New-Item -Force -Path "$PSScriptRoot\configs\$node_hub_server" -Name "node-$node_cwid.json" -ItemType "file" -Value $node_json_Template
    New-Item -Force -Path "$PSScriptRoot\configs\$node_hub_server" -Name "start-node-$node_cwid.bat" -ItemType "file" -Value $node_batch_Template

    $node_rdp_Template = [IO.File]::ReadAllText("$PSScriptRoot\templates\node_rdp_template.txt")
    $node_rdp_Template = $node_rdp_Template.Replace("[[ node_cwid ]]","$node_cwid")
    $node_rdp_Template = $node_rdp_Template.Replace("[[ node_hub_server ]]","$node_hub_server")
    New-Item -Force -Path "$PSScriptRoot\configs\$node_hub_server" -Name "rdp-$node_cwid.ps1" -ItemType "file" -Value $node_rdp_Template
  
}
